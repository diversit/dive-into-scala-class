package nextbuild

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class RestApiTest extends Simulation {

  val httpConf = http.baseURLs("http://localhost:8001")

  def createDynamic[T](max: Int)(f: Int => Map[String, T]) =
    (1 to max).map( f(_))

  def users(max: Int) = createDynamic(max) { nr => Map("user" -> s"user$nr") }.circular

  def tags(max: Int) = createDynamic(max) { nr => Map("tag" -> nr) }.random

  val singleUserScenario = scenario("Put single user tag")
      .exec(
        http("Put a request").put("/user/gatling/tag/1")
      )

  def randomPause: Duration = 100 + util.Random.nextInt(500) millis

  def multipleUsersScenario(loop: Int, nrUsers: Int, nrTags: Int) = scenario("Put multiple users with random tag")
      .repeat(loop) {
        feed(users(nrUsers))
        .feed(tags(nrTags))
        .exec(
          http("Put for random users with random tag")
            .put("/user/${user}/tag/${tag}")
        )
        .pause(randomPause)
      }


//  setUp(
//    singleUserScenario.inject(atOnceUsers(1))
//  ).protocols(httpConf)

  setUp(
    multipleUsersScenario(loop = 50, nrUsers = 20, nrTags = 5)
      .inject(rampUsers(1000) over (10 seconds))
  ).protocols(httpConf)
}
