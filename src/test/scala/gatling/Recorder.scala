package gatling

import io.gatling.recorder.GatlingRecorder
import io.gatling.recorder.config.RecorderPropertiesBuilder

/**
 * From Gatling-Maven archetype project.
 */
object Recorder extends App {

  val props = new RecorderPropertiesBuilder
  props.simulationOutputFolder(IDEPathHelper.recorderOutputDirectory.toString)
  props.simulationPackage("${package}")
  props.bodiesFolder(IDEPathHelper.bodiesDirectory.toString)

  GatlingRecorder.fromMap(props.build, Some(IDEPathHelper.recorderConfigFile))
}

import java.nio.file.Path

import io.gatling.core.util.PathHelper._

object IDEPathHelper {

  val gatlingConfUrl: Path = getClass.getClassLoader.getResource("gatling.conf").toURI
  val projectRootDir = gatlingConfUrl.ancestor(4)

  val testSourcesDirectory   = projectRootDir / "src" / "test" / "scala"
  val testResourcesDirectory = projectRootDir / "src" / "test" / "resources"
  val targetDirectory        = projectRootDir / "target"
  val testBinariesDirectory  = targetDirectory / "scala-2.11" / "test-classes"

  val dataDirectory          = testResourcesDirectory / "data"
  val bodiesDirectory        = testResourcesDirectory / "bodies"

  val recorderOutputDirectory = testSourcesDirectory
  val resultsDirectory = targetDirectory / "gatling-results"

  val recorderConfigFile = testResourcesDirectory / "recorder.conf"
}