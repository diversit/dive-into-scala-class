package nextbuild

import java.nio.file.Paths

import scala.io.Source
import scala.util.Try

object LogParser extends App {

  val file = Paths.get("epa-http.txt")
  val lines = Source.fromFile(file.toFile).getLines

  val regex = """(.*)\s\[(\d{2}:\d{2}:\d{2}:\d{2})\]\s\"(GET|POST|HEAD)\s(.*)\sHTTP/1.0\"\s(\d{3})\s(\d{1,}|-)""".r

  val res = lines map { line =>

    Try {
      val regex(client, ip, method, url, result, size) = line
      println(s"client: $client, ip:$ip, method:$method, result: $result, size:$size")

    }
  } toList

  res foreach println

//  val (successes, failures) = res.foldLeft((0,0))((acc,v) => if (v.isSuccess) (acc._1 + 1, acc._2) else (acc._1, acc._2 + 1))
  val (successes, failures) = res.foldLeft((0,0)) {
    case ((ss,fs),v) => if (v.isSuccess) (ss + 1, fs) else (ss, fs + 1)
  }

  println(s"Successes:${successes}")
  println(s"Failures :${failures}")
}
