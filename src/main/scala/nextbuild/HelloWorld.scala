package nextbuild

import scala.annotation.tailrec
import scala.io.StdIn

object HelloWorld extends App {

  @tailrec
  def getName: String = {
    val name = if (args.length > 0) args(0) else StdIn.readLine("Enter a name:")
    if (name.trim.length > 0) name else getName
  }

  println(s"Hello $getName")
}
