package nextbuild

/**
 * Created by joostdenboer on 29/05/15.
 */
package object rest {

  type Tag = String
  type Count = Int

  object JsonFormat {

    import org.json4s._
    import org.json4s.native.Serialization

    object NoneJNullSerializer extends CustomSerializer[Option[_]](format => ({ case JNull => None }, { case None => JNull }))

    implicit val jsonFormats: Formats = Serialization.formats(NoTypeHints) + NoneJNullSerializer
  }

  implicit class AnyRefToJson(val any: AnyRef) {

    import org.json4s.native.Serialization._
    import JsonFormat._

    def toJson: String = writePretty(any)
  }

}
