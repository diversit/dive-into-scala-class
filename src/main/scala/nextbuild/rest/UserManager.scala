package nextbuild.rest

import akka.actor._
import akka.contrib.pattern.ClusterSharding
import nextbuild.rest.UserActorProtocol.{UserSummary, Summarize, CountTag}
import nextbuild.rest.UserManagerProtocol.{UserEnvelop, AllUsersSummary, UserNotFound}

import scala.reflect.ClassTag

trait UserManager {

  val usersRegion: ActorRef
  var users = Set.empty[String]

  def forwardToUser(username: String, tag: String): Unit = {
    users += username
    usersRegion ! UserEnvelop(username, CountTag(tag))
  }

  def replySummaryOfUserTo(replyTarget: ActorRef, username: String): Unit = users(username) match {
    case true  => usersRegion.tell(UserEnvelop(username, Summarize), replyTarget)
    case false => replyTarget ! UserNotFound(username)
  }

  def replyAllSummariesTo(replyActor: ActorRef): Unit = {
    if (users.isEmpty) {
      replyActor ! AllUsersSummary(0, List.empty[UserSummary])
    } else {
      // Extra Pattern --> Effective Akka book
      createExtra(new Actor with ActorLogging {
        // capture size beforehand since it might change when additional counts are received while waiting for summaries
        val totalUsers = users.size
        // tell each user to summerize
        users.foreach(user => usersRegion ! UserEnvelop(user, Summarize))

        // gather all replies in a map
        var summaries = Map.empty[String, UserSummary]

        override def receive: Actor.Receive = {
          case userSummary @ UserSummary(user, total, summary) =>
            log.debug(s"Received summary of user '$user'")
            summaries += (user -> userSummary)

            if (summaries.size == totalUsers) {
              log.info("Received all summaries")
              val total = summaries.values.map(_.totalCount).sum

              replyActor ! AllUsersSummary(total, summaries.values.toList)

              // stop this 'extra' actor
              context.stop(self)
            }
        }
      })
    }
  }

  def createExtra[T <: Actor : ClassTag](creator: => T): ActorRef
}

class UserManagerActor extends Actor with UserManager with ActorLogging {

  import UserManagerProtocol._

  override val usersRegion: ActorRef = ClusterSharding(context.system).shardRegion(User.USERS_SHARD)

  override def createExtra[T <: Actor : ClassTag](creator: => T): ActorRef = context.actorOf(Props(creator))

  override def receive: Receive = {
    case CountTagForUser(tag, username) => forwardToUser(username, tag)
    case SummarizeAllUsers => replyAllSummariesTo(sender)
    case SummarizeUser(username) => replySummaryOfUserTo(sender, username)
  }
}

object UserManagerActor {

  def props = Props[UserManagerActor]
}

object UserManagerProtocol {
  case class CountTagForUser(tag: Tag, username: String)
  case object SummarizeAllUsers
  case class SummarizeUser(username: String)
  case class AllUsersSummary(total: Count, users: List[UserSummary])
  case class UserNotFound(unknownUser: String)

  /**
   * The envelop containing the user message send to a sharded user.
   * @param username
   * @param payload
   */
  case class UserEnvelop(username: String, payload: AnyRef)
}
