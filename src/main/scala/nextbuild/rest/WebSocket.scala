package nextbuild.rest

import akka.actor.{Actor, ActorLogging, Props}
import unfiltered.netty.websockets._

import scala.collection.mutable.ListBuffer

/**
 * Define WebSocket functionality
 */
trait WebSocketManager {
  self: ActorLogging =>

  val sockets = ListBuffer.empty[WebSocket]

  def addSocket(socket: WebSocket) = {
    sockets += socket
    socket.send("""{ "connection": "accepted"} """)
    log.info(s"Added socket: $socket")
  }

  def removeSocket(socket: WebSocket) = {
    sockets -= socket
    socket.send("""{ "connection" : "closed" }""")
    log.info(s"Removed socket: $socket")
  }

  def handleMessageFromSocket(socket: WebSocket, msg: Msg) = msg match {
    case Text(txt) if (txt == "close") => removeSocket(socket)
    case other => log.info(s"Received some other message from socket $socket: $other")
  }

  def sendToSockets(msg: AnyRef) = {
    val json = msg.toJson
//    log.info(s"Sending to all sockets: $json")
    sockets foreach (_.send(json))
  }
}

class WebSocketManagerActor extends Actor with ActorLogging with WebSocketManager {

  override def receive: Receive = {
    case Open(socket)  => addSocket(socket)
    case Close(socket) => removeSocket(socket)
    case Error(socket, error) =>
      log.error(s"Error on socket")
      removeSocket(socket)
    case Message(socket, msg) => handleMessageFromSocket(socket, msg)
    case other: AnyRef => sendToSockets(other)
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    super.postStop()
  }
}

object WebSocketActor {
  def props = Props[WebSocketManagerActor]
}
