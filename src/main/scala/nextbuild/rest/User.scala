package nextbuild.rest

import akka.actor.{ActorRef, ActorLogging, Props}
import akka.contrib.pattern.ShardRegion
import akka.persistence.PersistentActor
import nextbuild.rest.UserActorProtocol.{Summarize, UserSummary, TagCount, CountTag}
import nextbuild.rest.UserManagerProtocol.UserEnvelop


object User {

  val USERS_SHARD = "users"

  val userIdExtractor: ShardRegion.IdExtractor = {
    case UserEnvelop(username, payload) => (username, payload)
  }

  val userShardResolver: ShardRegion.ShardResolver = {
    case UserEnvelop(username, _) => (username.hashCode % 12).toString
  }

  case class UserState(tagsWithCount: Map[Tag, Count]) {
    def increment(tag: Tag): UserState = {
      val newCount = tagsWithCount.getOrElse(tag, 0) + 1
      copy(tagsWithCount + (tag -> newCount))
    }

    def tagCount(tag: Tag): TagCount = TagCount(tag, tagsWithCount.getOrElse(tag, 0))

    def totalCount: Count = tagsWithCount.values.sum

    def tagCounts: List[TagCount] = tagsWithCount.map {
      case (tag, count) => TagCount(tag, count)
    }.toList
  }

  case class TagEvent(tag: Tag)
}

trait User {
  self: ActorLogging =>

  import User._

  val username: String
  var state = UserState(Map.empty)

  def countTag(event: TagEvent): Unit = {
    val tag = event.tag
    state = state.increment(tag)
    log.info(s"User $username: counted ${state.tagCount(tag).count} for tag $tag")
  }

  def replySummaryTo(replyTarget: ActorRef): Unit =
      replyTarget ! UserSummary(username, state.totalCount, state.tagCounts)
}

class UserActor extends PersistentActor with User with ActorLogging {

  val username = self.path.parent.name + "-" + self.path.name
  import User._

  override def persistenceId: String = username

  override def receiveRecover: Receive = {
    case event: TagEvent =>
      log.info(s"Restoring: event")
      countTag(event)
//    case SnapshotOffer(_, offer: UserState) => state = offer
  }

  override def receiveCommand: Receive = {
    case CountTag(tag) => persist(TagEvent(tag))(countTag) // persistAsync(..)(..)
    case Summarize     => replySummaryTo(sender)
  }
}

object UserActor {

  def props = Props[UserActor]
}

object UserActorProtocol {

  case class CountTag(tag: Tag)
  case object Summarize
  case class UserSummary(username: String, totalCount: Int, tagCounts: List[TagCount])
  case class TagCount(tag: Tag, count: Count)
}