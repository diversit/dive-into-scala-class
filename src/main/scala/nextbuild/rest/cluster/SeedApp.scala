package nextbuild.rest.cluster

import akka.actor.ActorSystem
import akka.contrib.pattern.ClusterSharding
import com.typesafe.config.ConfigFactory
import nextbuild.rest.{UserActor, User}

/**
 * Starts a seed node.
 */
object SeedApp extends App {

  val port = if (args.length == 0) "2551" else args(0)

  val config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
    withFallback(ConfigFactory.load())

  val system = ActorSystem("RestApp", config)

  SharedJournal.startup(system, port)

  ClusterSharding(system).start(
    typeName      = User.USERS_SHARD,
    entryProps    = Some(UserActor.props),
    idExtractor   = User.userIdExtractor,
    shardResolver = User.userShardResolver
  )

  sys.addShutdownHook {
    system.shutdown()
  }
}
