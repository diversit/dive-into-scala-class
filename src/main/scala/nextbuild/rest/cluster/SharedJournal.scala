package nextbuild.rest.cluster

import akka.actor._
import akka.persistence.journal.leveldb.SharedLeveldbJournal
import akka.persistence.journal.leveldb.SharedLeveldbStore
import akka.util.Timeout
import concurrent.duration._
import akka.pattern.ask

/**
 * Created by joost on 23/06/15.
 */
object SharedJournal {

//  val storePath = ActorPath.fromString("akka://RestApp/user/store")
  val storePath = ActorPath.fromString("akka.tcp://RestApp@127.0.0.1:2551/user/store")

  def startup(system: ActorSystem, port: String): Unit = {
    // Start the shared journal one one node (don't crash this SPOF)
    // This will not be needed with a distributed journal
    if (port == "2551") {
      system.log.info("Starting shared store.")
      system.actorOf(Props[SharedLeveldbStore], "store")
    }
    // register the shared journal
    import system.dispatcher
    implicit val timeout = Timeout(15.seconds)
    val f = (system.actorSelection(storePath) ? Identify(None))
    f.onSuccess {
      case ActorIdentity(_, Some(ref)) =>
        system.log.info(s"Shared journal found at ref '$ref'")
        SharedLeveldbJournal.setStore(ref, system)
      case _ =>
        system.log.error(s"Shared journal not started at $storePath")
        system.shutdown()
    }
    f.onFailure {
      case _ =>
        system.log.error(s"Lookup of shared journal at $storePath timed out")
        system.shutdown()
    }
  }
}
