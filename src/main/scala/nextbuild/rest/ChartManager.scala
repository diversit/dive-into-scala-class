package nextbuild.rest

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import nextbuild.rest.UserActorProtocol.{TagCount, UserSummary}
import nextbuild.rest.UserManagerProtocol.{AllUsersSummary, SummarizeAllUsers}

/**
 * Manages sending chart data to websockets.
 *
 * Gathers summary from [[UserManagerActor]] every x seconds,
 * transforms the result into data which can be fed to chart directly.
 * Sends the result to the [[WebSocketManagerActor]] so it gets distributed to all clients.
 */
class ChartManagerActor(val userManagerActor: ActorRef, val websocketManagerActor: ActorRef)
      extends Actor with ActorLogging {

  import ChartManagerActor._
  import ChartManagerActorProtocol._

  import concurrent.ExecutionContext.Implicits.global
  import concurrent.duration._

  var timer = context.system.scheduler.scheduleOnce(5 seconds, self, GatherSummary)

  override def receive: Receive = {
    case GatherSummary =>
      log.info("Gathering summaries ...")
      userManagerActor ! SummarizeAllUsers
    case AllUsersSummary(_, userSummaries) =>
      log.info("Transforming summaries to data...")
      transformToChartData(userSummaries) { data =>
        log.info("Distributing data to websockets...")
        websocketManagerActor ! data
      }
      timer = context.system.scheduler.scheduleOnce(5 seconds, self, GatherSummary)
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    if (!timer.isCancelled) timer.cancel()
    super.postStop()
  }
}

object ChartManagerActor {

  def props(userManagerActor: ActorRef, webSocketManagerActor: ActorRef) =
        Props(new ChartManagerActor(userManagerActor, webSocketManagerActor))

  import ChartManagerActorProtocol._

  def transformToChartData(uss: List[UserSummary])(f: Chart => Unit): Unit = {
    if (uss.nonEmpty) {
      // Get all unique tags
      val tags: Set[String] = uss.flatMap(_.tagCounts.map(_.tag)).toSet

      // for each tag, find the count for each user (== None if no found)
      val series = tags map { tag =>
        val data: List[Option[Int]] = uss.map { us =>
          us.tagCounts.find(_.tag == tag).map(_.count)
        }
        ChartSerie(tag, data)
      }

      // Get all usernames
      val users = uss.map(_.username)

      f(Chart(ChartData(users, series)))
    }
  }
}

object ChartManagerActorProtocol {
  case object GatherSummary
  case class Chart(chart: ChartData)
  case class ChartData(categories: List[String], series: Set[ChartSerie])
  case class ChartSerie(name: String, data: List[Option[Int]])
}

object TestTransformToChartData extends App {

  val uss = List(
    UserSummary("user1", 10, List(TagCount("tag1", 11), TagCount("tag2", 12))),
    UserSummary("user2", 10, List(TagCount("tag1", 21), TagCount("tag3", 23))),
    UserSummary("user3", 10, List(TagCount("tag2", 32), TagCount("tag3", 33)))
  )

  ChartManagerActor.transformToChartData(uss) { c =>
    println(c)
    println(c.toJson)
  }

}
