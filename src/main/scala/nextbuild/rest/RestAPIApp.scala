package nextbuild.rest

import akka.actor.ActorSystem
import akka.contrib.pattern.ClusterSharding
import akka.util.Timeout
import nextbuild.rest.UserActorProtocol.UserSummary
import nextbuild.rest.UserManagerProtocol.{SummarizeUser, AllUsersSummary, SummarizeAllUsers, CountTagForUser}
import nextbuild.rest.cluster.SharedJournal
import unfiltered.Async
import unfiltered.response._
import unfiltered.request._

import scala.concurrent.Future
import scala.util.{Failure, Success}
import concurrent.duration._

object RestAPIApp extends App {

  implicit val askTimeout = Timeout(1 second)
  import concurrent.ExecutionContext.Implicits.global
  import akka.pattern.ask

  val system = ActorSystem("RestApp")

  SharedJournal.startup(system, "0")

  ClusterSharding(system).start(
    typeName      = User.USERS_SHARD,
    entryProps    = Some(UserActor.props),
    idExtractor   = User.userIdExtractor,
    shardResolver = User.userShardResolver
  )

  val userManager = system.actorOf(UserManagerActor.props, "usermanager")
  val websocket = system.actorOf(WebSocketActor.props, "websocket")
  val chartManager = system.actorOf(ChartManagerActor.props(userManager, websocket), "chartmanager")

  def reply[A](rf: ResponseFunction[A])(implicit responder: Async.Responder[A]): Unit = responder.respond(rf)
  def async[A](frf: Future[ResponseFunction[A]])(implicit responder: Async.Responder[A]): Unit = frf.onComplete {
    case Success(rf) => reply(rf)
    case Failure(error) => reply(InternalServerError ~> ResponseString(s"Error: ${error.getMessage}"))
  }

  val api = unfiltered.netty.async.Planify { case req =>
    implicit val responder = req
    req match {
      case GET(Path("/hello")) => reply(ResponseString("World\n"))
      case GET(Path(Seg("hello" :: name :: Nil))) => reply(ResponseString(s"$name\n"))
      case PUT(Path(Seg("user" :: username :: "tag" :: tag :: Nil))) =>
        userManager ! CountTagForUser(tag, username)
        reply(Created ~> ResponseString(s"Counting tag $tag for user $username\n"))
      case GET(Path("/user/summary")) => async {
        (userManager ? SummarizeAllUsers).mapTo[AllUsersSummary]
          .map { summary => JsonContent ~> ResponseString(summary.toJson) }
      }
      case GET(Path(Seg("user" :: username :: "summary" :: Nil))) => async {
        (userManager ? SummarizeUser(username)).mapTo[UserSummary]
          .map { summary => JsonContent ~> ResponseString(summary.toJson) }
      }
      case _ => reply(ResponseString("Hello World\n"))
    }
  }

  val websocketPlan = unfiltered.netty.websockets.Planify {
    case Path("/ws") => {
      case any => websocket ! any
    }
  }

  unfiltered.netty.Server.http(8001)
      .plan(websocketPlan.onPass(_.fireChannelRead(_)))
      .plan(api).run { server =>
    println("Server started.\n")
  }
}
